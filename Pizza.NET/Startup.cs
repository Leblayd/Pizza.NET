﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PizzaDotNet.Startup))]
namespace PizzaDotNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
