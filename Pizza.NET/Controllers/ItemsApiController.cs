﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using PizzaDotNet.Models;

namespace PizzaDotNet.Controllers
{
    public class ItemsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Items?search=pizza
        public IHttpActionResult GetItems(string search)
        {
            var menuItems = db.MenuItems
                .Include(mi => mi.Category.MenuItems)
                .Where(mi => mi.Name.Contains(search) || mi.Description.Contains(search)) // TODO move search logic to MenuItem
                // Necessary to be able to use an expression tree below
                .AsEnumerable()
                // Get rid of circular reference in mi.Category.MenuItems
                .Select(mi => { mi.Category.MenuItems = null; return mi; });

            var categories = db.Categories
                .Where(cat => cat.Name.Contains(search))
                .Include(mi => mi.MenuItems);

            return Ok(new { menuItems, categories });
        }
    }
}